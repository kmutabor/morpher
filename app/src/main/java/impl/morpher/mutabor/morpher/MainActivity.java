package impl.morpher.mutabor.morpher;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


/**
 * Программа использует API сайта https://ws3.morpher.ru/ (бесплатную версию, количество запросов
 * с одного ip ограничено 1000) см. http://morpher.ru/ws3/
 */
public class MainActivity extends AppCompatActivity {

    final String ATTRIBUTE_PADEZH = "padezh";
    final String ATTRIBUTE_SLOVO = "slovo";
    final String sourceBaseUrl = "https://ws3.morpher.ru/russian/";
    /**
     * Параметр для обработки слов
     */
    final String sParam = "declension?s=";
    /**
     * Параметр для обработки чисел
     */
    final String nParam = "spell?n=";

    /**
     * Возвращаемый формат от сервера. По умолчанию возвращает xml
     * Програма раотает с json
     */
    final String jsonParam = "&format=json";

    final String[] from = {ATTRIBUTE_PADEZH, ATTRIBUTE_SLOVO};
    ;
    final int[] to = {R.id.padezh, R.id.slovo};
    /**
     * Данные, передаваемые в адаптер для отображения
     */
    private ArrayList<Map<String, Object>> data;

    private ListView lvText;

    private Button btnMorph;

    private SimpleAdapter sAdapter;

    private EditText eText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        data = new ArrayList<Map<String, Object>>(0);
        sAdapter = new SimpleAdapter(this, data, R.layout.item, from, to);

        lvText = (ListView) findViewById(R.id.result);
        lvText.setAdapter(sAdapter);

        btnMorph = (Button) findViewById(R.id.morph);
        btnMorph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eText = (EditText) findViewById(R.id.inputText);
                String query = eText.getText().toString();
                if(query.isEmpty()){
                    return;
                }
                new ParseTask(query).execute();
            }
        });
    }


    /**
     * Выполянет работу по запроса и ответа сервера
     * Обновляет значение в адаптере
     */
    private class ParseTask extends AsyncTask<Void, Void, String> {

        ParseTask(String query) {
            queryString = query;
            queryString = URLEncoder.encode(queryString);
            if (isInteger(queryString)) {
                numQuery = true;
                requestType = nParam;
                queryString = queryString + "&unit=" + URLEncoder.encode("рубль");
            } else {
                requestType = sParam;
            }
            sourceString = sourceBaseUrl + requestType + queryString + jsonParam;
        }

        private String queryString = null;
        private String sourceString = null;
        private BufferedReader reader = null;
        private String resultJson = null;
        private String requestType = null;
        private boolean numQuery = false;

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL(sourceString);
                HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                resultJson = buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return resultJson;
        }


        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            if (strJson == null) {
                return;
            }

            data.clear();
            try {
                JSONObject dataJsonObj;
                if (numQuery) {
                    dataJsonObj = new JSONObject(strJson).getJSONObject("n");
                    addLineToResultDataArray(dataJsonObj.getString("И"), "И");
                    addLineToResultDataArray(dataJsonObj.getString("Р"), "Р");
                    addLineToResultDataArray(dataJsonObj.getString("Д"), "Д");
                    addLineToResultDataArray(dataJsonObj.getString("В"), "В");
                    addLineToResultDataArray(dataJsonObj.getString("П"), "П");
                    addLineToResultDataArray(dataJsonObj.getString("П_о"), "П_о");
                } else {
                    dataJsonObj = new JSONObject(strJson);
                    addLineToResultDataArray(URLDecoder.decode(queryString, "UTF-8"), "И");
                    addLineToResultDataArray(dataJsonObj.getString("Р"), "Р");
                    addLineToResultDataArray(dataJsonObj.getString("Д"), "Д");
                    addLineToResultDataArray(dataJsonObj.getString("В"), "В");
                    addLineToResultDataArray(dataJsonObj.getString("Т"), "Т");
                    addLineToResultDataArray(dataJsonObj.getString("П"), "П");

                    dataJsonObj = dataJsonObj.getJSONObject("множественное");
                    addLineToResultDataArray(dataJsonObj.getString("И"), "И.мн.");
                    addLineToResultDataArray(dataJsonObj.getString("Р"), "Р.мн.");
                    addLineToResultDataArray(dataJsonObj.getString("Д"), "Д.мн.");
                    addLineToResultDataArray(dataJsonObj.getString("В"), "В.мн.");
                    addLineToResultDataArray(dataJsonObj.getString("Т"), "Т.мн.");
                    addLineToResultDataArray(dataJsonObj.getString("П"), "П.мн.");
                }

            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
            } finally {
                sAdapter.notifyDataSetChanged();
            }
        }
    }


    /**
     * Доабвляет строки в адаптер для представления
     *
     * @param word
     * @param key
     */
    private void addLineToResultDataArray(String word, String key) {
        Map<String, Object> m = new HashMap<String, Object>();
        m.put(ATTRIBUTE_PADEZH, key);
        m.put(ATTRIBUTE_SLOVO, word);
        data.add(m);
    }

    /**
     * Проверка аргумента на число
     *
     * @param input
     * @return
     */
    private boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
